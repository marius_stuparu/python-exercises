# Write your code here
import sys


class CoffeeMachine:
    espresso = {
        'water': 250,
        'milk': 0,
        'beans': 16,
        'price': 4
    }

    latte = {
        'water': 350,
        'milk': 75,
        'beans': 20,
        'price': 7
    }

    cappuccino = {
        'water': 200,
        'milk': 100,
        'beans': 12,
        'price': 6
    }

    def __init__(self):
        self.waterReserve = 400
        self.milkReserve = 540
        self.beansReserve = 120
        self.cupsReserve = 9
        self.moneyBalance = 550
        self.__userActionInput()

    def __printMachineState(self):
        """Print the machine reserve status"""
        print(f"""The coffee machine has:
                {self.waterReserve} of water
                {self.milkReserve} of milk
                {self.beansReserve} of coffee beans
                {self.cupsReserve} of disposable cups
                {self.moneyBalance} of money\n""")

    def __userActionInput(self):
        """User action input"""
        action = ''
        actions = ['buy', 'fill', 'take', 'exit', 'remaining']
        while action not in actions:
            action = input(f'Write action ({", ".join(actions)}):')

        if action == 'buy':
            self.__buyCoffee()
        elif action == 'fill':
            self.__fillMchine()
        elif action == 'take':
            self.__takeMoney()
        elif action == 'remaining':
            self.__printMachineState()
            self.__userActionInput()
        elif action == 'exit':
            sys.exit()

    def __canMake(self, coffeeType):
        """If there are enough resources to make a coffee"""
        missingResources = []

        if self.waterReserve < coffeeType['water']:
            missingResources.append('water')

        if self.milkReserve < coffeeType['milk']:
            missingResources.append('milk')

        if self.beansReserve < coffeeType['beans']:
            missingResources.append('coffee beans')

        if self.cupsReserve < 1:
            missingResources.append('disposable cups')

        return missingResources

    def __makeCoffee(self, coffeeType):
        """Make one coffee"""
        self.moneyBalance += coffeeType['price']
        self.waterReserve -= coffeeType['water']
        self.milkReserve -= coffeeType['milk']
        self.beansReserve -= coffeeType['beans']
        self.cupsReserve -= 1

    def __buyCoffee(self):
        """User input coffee type"""
        coffeeOptions = {
            '1': self.espresso,
            '2': self.latte,
            '3': self.cappuccino,
            'back': None
        }
        coffeeInput = 0
        while coffeeInput not in coffeeOptions.keys():
            coffeeInput = input(
                'What do you want to buy? 1 - espresso, 2 - latte, 3 - cappuccino, back - to main menu:')

        if coffeeInput == 'back':
            self.__userActionInput()

        missingResources = self.__canMake(coffeeOptions[coffeeInput])
        if len(missingResources) == 0:
            print('I have enough resources, making you a coffee!\n')
            self.__makeCoffee(coffeeOptions[coffeeInput])
            self.__userActionInput()
        else:
            print(f'Sorry, not enough {", ".join(missingResources)}!')
            self.__userActionInput()

    def __fillMchine(self):
        self.waterReserve += int(input('Write how many ml of water do you want to add:'))
        self.milkReserve += int(input('Write how many ml of milk do you want to add:'))
        self.beansReserve += int(input('Write how many grams of coffee beans do you want to add:'))
        self.cupsReserve += int(input('Write how many disposable cups of coffee do you want to add:'))
        self.__userActionInput()

    def __takeMoney(self):
        print(f'I gave you ${self.moneyBalance}')
        self.moneyBalance = 0
        self.__userActionInput()


machine = CoffeeMachine()
