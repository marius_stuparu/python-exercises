# write your code here
from typing import List, Tuple
import os


class TicTacToe:
    def __init__(self):
        self.board = [
            ['(1 3)', '(2 3)', '(3 3)'],
            ['(1 2)', '(2 2)', '(3 2)'],
            ['(1 1)', '(2 1)', '(3 1)']
        ]
        self.xCount = 0
        self.oCount = 0
        self.emptyCount = 9
        self.players = {1: 'X', -1: 'O'}
        self.currentPlayer = 1
        self.__main()

    def __main(self):
        """The main sequence"""
        self.__printStatus()
        self.__inputMove()

    def __printStatus(self):
        """Display the current board status"""
        os.system('clear')
        print(f'{chr(0x250C)}{chr(0x2500) * 7}{chr(0x252C)}{chr(0x2500) * 7}{chr(0x252C)}{chr(0x2500) * 7}{chr(0x2510)}')
        for row in range(0, 3):
            if row > 0:
                print(f'{chr(0x251C)}{chr(0x2500) * 7}{chr(0x253C)}{chr(0x2500) * 7}{chr(0x253C)}{chr(0x2500) * 7}{chr(0x2524)}')
            print(f'{chr(0x2502)}{" " * 7}{chr(0x2502)}{" " * 7}{chr(0x2502)}{" " * 7}{chr(0x2502)}')
            print(f'{chr(0x2502)}'
                  f'{self.board[row][0].center(7)}{chr(0x2502)}'
                  f'{self.board[row][1].center(7)}{chr(0x2502)}'
                  f'{self.board[row][2].center(7)}{chr(0x2502)}')
            print(f'{chr(0x2502)}{" " * 7}{chr(0x2502)}{" " * 7}{chr(0x2502)}{" " * 7}{chr(0x2502)}')
        print(f'{chr(0x2514)}{chr(0x2500) * 7}{chr(0x2534)}{chr(0x2500) * 7}{chr(0x2534)}{chr(0x2500) * 7}{chr(0x2518)}')

    def __inputMove(self):
        correctInput = False

        while not correctInput and not self.__checkGameEnded():
            humanInput = input(f'Player {self.players[self.currentPlayer]}\'s move: ')
            (humanX, humanY) = humanInput.split()

            try:
                if not humanX.isnumeric() or not humanY.isnumeric():
                    raise TypeError('You should enter numbers!')

                humanX = int(humanX)
                humanY = int(humanY)

                if humanX not in range(1, 4) or humanY not in range(1, 4):
                    raise OverflowError('Coordinates should be from 1 to 3!')

                matrixRow = abs(humanY - 3)
                matrixCol = humanX - 1

                if self.board[matrixRow][matrixCol] == 'X' or self.board[matrixRow][matrixCol] == 'O':
                    raise PermissionError('This cell is occupied! Choose another one')

                self.board[matrixRow][matrixCol] = self.players[self.currentPlayer]
                self.emptyCount -= 1
                self.__printStatus()
                correctInput = True
            except TypeError as te:
                print(te)
            except OverflowError as oe:
                print(oe)
            except PermissionError as pe:
                print(pe)
            finally:
                if not self.__checkGameEnded():
                    self.currentPlayer *= -1
                    self.__inputMove()

    def __checkGameEnded(self) -> bool:
        """Check if the board is correct, and see if there's a winner"""
        gameEnded = False
        if self.__checkValidity():
            xLines = self.__checkWinner('X')
            oLines = self.__checkWinner('O')
            if xLines >= 1 and oLines == 0:
                print('X wins')
                gameEnded = True
            elif oLines >= 1 and xLines == 0:
                print('O wins')
                gameEnded = True
            elif self.emptyCount == 0 and xLines == 0 and oLines == 0:
                if self.emptyCount == 0:
                    print('Draw')
                    gameEnded = True

        return gameEnded

    def __checkValidity(self) -> bool:
        """Check if board is valid - X is always first, so it has equal or one more moves than O"""
        return self.xCount == self.oCount + 1 or self.xCount == self.oCount

    def __checkWinner(self, player: str) -> int:
        """Evaluate possible winning combinations and return number of matched lines"""
        matches = [[(0, 0), (0, 1), (0, 2)], [(1, 0), (1, 1), (1, 2)], [(2, 0), (2, 1), (2, 2)],
                   [(0, 0), (1, 0), (2, 0)], [(0, 1), (1, 1), (2, 1)], [(0, 2), (1, 2), (2, 2)],
                   [(0, 0), (1, 1), (2, 2)], [(0, 2), (1, 1), (2, 0)]]
        matchedLines = 0
        for possibleMatches in matches:
            if (player == self.board[possibleMatches[0][0]][possibleMatches[0][1]]) and \
                    (player == self.board[possibleMatches[1][0]][possibleMatches[1][1]]) and \
                    (player == self.board[possibleMatches[2][0]][possibleMatches[2][1]]):
                matchedLines += 1
        return matchedLines


if __name__ == '__main__':
    ticTacToe = TicTacToe()
