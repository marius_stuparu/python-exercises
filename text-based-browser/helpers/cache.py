#!/usr/bin/env python3

import os
from typing import List, Optional, TextIO


class Cache:
    cacheFolder = ''
    cachedPages = []

    def __init__(self, cacheFolder: str):
        """Initialize the cache folder and read existing caches"""
        try:
            self.cacheFolder = os.path.abspath(cacheFolder)
            if not os.path.exists(self.cacheFolder):
                os.makedirs(self.cacheFolder, exist_ok=True)
            else:
                cached = os.listdir(self.cacheFolder)
                for file in cached:
                    self.cachedPages.append(file)
        except IOError as io_err:
            print(io_err)

    def __readCache(self, pageName: str) -> Optional[List[str]]:
        """Fetch the cached page from disc"""
        cacheFile: TextIO
        try:
            with open(os.path.join(
                    str(self.cacheFolder),
                    str(pageName)),
                    'r') as cacheFile:
                return cacheFile.readlines()
        except IOError as io_err:
            print(io_err)
            return None

    def __saveCache(self, pageName: str, pageContent: str) -> bool:
        """Save a new page to the cache store"""
        cacheFile: TextIO
        try:
            with open(os.path.join(
                    str(self.cacheFolder),
                    str(pageName)),
                    'w+') as cacheFile:
                cacheFile.write(pageContent)
        except IOError as io_err:
            print(io_err)
            return False
        else:
            return True

    def cacheCheck(self, pageName: str) -> Optional[List[str]]:
        """If it's a cache hit, return it"""
        if pageName in self.cachedPages:
            return self.__readCache(pageName)
        else:
            return None

    def cacheSave(self, pageName: str, pageContent: str) -> bool:
        """Save a new page to the cache store"""
        if self.__saveCache(pageName, pageContent):
            self.cachedPages.append(pageName)
            return True
        else:
            return False


if __name__ == '__main__':
    pass
