#!/usr/bin/env python3

from typing import List, Optional


class History:
    backHistory: List[str]

    def __init__(self):
        """Initialize browser history"""
        self.backHistory = []

    def pushNewHistory(self, pageName) -> bool:
        """Add new page to browsing history"""
        self.backHistory.append(pageName)
        return True

    def popHistory(self) -> Optional[str]:
        """Navigate back, return previous page name"""
        if len(self.backHistory):
            pageName = self.backHistory.pop()
            return pageName
        else:
            return None


if __name__ == '__main__':
    pass
