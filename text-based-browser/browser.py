import sys
import requests as http
from urllib.parse import urlparse, ParseResult
from bs4 import BeautifulSoup
from colorama import Fore as colorF, Style as colorS

# from .helpers import cache, history
from helpers import cache, history


# write your code here
class TextBrowser:
    currentUrl = ''
    cache = None
    history = None
    tagBlacklist = [
        '[document]', 'noscript', 'header', 'html', 'meta', 'head', 'input', 'script', 'link', 'style'
    ]

    @staticmethod
    def getPage(url: str) -> str:
        """Make a GET request and return the text of the page"""
        req = http.get(url)
        if req and req.text:
            return req.text
        else:
            return ''

    def __init__(self, storage='caches'):
        self.cache = cache.Cache(storage)
        self.history = history.History()
        self.inputUrl()

    def inputUrl(self):
        """Input commands from user.
           Type back for going back in browsing history,
           or exit to close the browser"""
        newUrl = input('URL, shortcut, back or exit: ')
        if newUrl == 'exit':
            sys.exit()
        elif newUrl == 'back':
            self.currentUrl = self.history.popHistory()
            if self.currentUrl:
                self.__checkURL()
            else:
                self.inputUrl()
        else:
            self.history.pushNewHistory(self.currentUrl)
            self.currentUrl = newUrl
            self.__checkURL()

    def __checkURL(self):
        """Check if page is already in the cache or is a valid URL"""
        if self.currentUrl.count('.') == 0:
            cacheCheck = self.cache.cacheCheck(self.currentUrl)
            if cacheCheck:
                for line in cacheCheck:
                    line = line.strip('\n')
                    print(line)
            else:
                print('Error: Incorrect cached URL')

            self.inputUrl()
        else:
            self.__loadNewDomain()

    def __parseHtml(self, content: str) -> str:
        """Parse the HTML markup and extract the text.
           Colorize the links in blue"""
        soup = BeautifulSoup(content, 'html.parser')
        output = ''
        text = soup.find_all(text=True)
        for tag in text:
            if tag.parent.name not in self.tagBlacklist:
                if tag.parent.name == 'a':
                    output += colorF.BLUE + '{} '.format(tag) + colorS.RESET_ALL
                else:
                    output += '{} '.format(tag)
        return output

    def __loadNewDomain(self):
        """Load a newly observed domain"""
        parsed: ParseResult
        parsed = urlparse(self.currentUrl)
        hostname = ''

        if parsed.netloc:
            hostname = parsed.netloc
        elif parsed.path:
            hostname = parsed.path

        if not parsed.scheme:
            self.currentUrl = 'https://' + self.currentUrl

        content = self.getPage(self.currentUrl)
        parsedContent = self.__parseHtml(content)

        shortcut = hostname.split('.')

        if self.cache.cacheSave(shortcut[0], parsedContent):
            print(parsedContent)
        self.inputUrl()


if __name__ == '__main__':
    args = sys.argv
    if len(args) == 2:
        browser = TextBrowser(args[1])
    else:
        browser = TextBrowser()
