import sys
from pprint import pprint
from typing import List


class Matrices:
    matrixA = []
    matrixB = []
    result = []

    def __init__(self):
        self.uiMenu()

    @staticmethod
    def __inputMatrix(matrixID: str) -> []:
        """Input the matrix dimensions and rows from user
           and return a properly formatted array"""
        if matrixID:
            matrixID = ' ' + matrixID

        dimensions = input(f'Enter size of{matrixID} matrix: ').split(' ')

        if len(dimensions) == 1:
            rows = cols = int(dimensions[0])
        else:
            rows = int(dimensions[0])
            cols = int(dimensions[1])

        returnMatrix = [[0 for col in range(cols)] for row in range(rows)]

        print(f'Enter{matrixID} matrix:')
        for row in range(rows):
            rowInput = input().split(' ')
            if len(rowInput) == cols:
                for col in range(cols):
                    new = rowInput[col]
                    if new.isdigit():
                        returnMatrix[row][col] = int(new)
                    else:
                        returnMatrix[row][col] = float(new)
            else:
                raise ValueError(f'Too many columns for matrix{matrixID}')
        return returnMatrix

    @staticmethod
    def __inputNumber() -> float:
        """User input the scalar multiplication number"""
        scalar = input('Enter constant: ')
        if scalar.isdigit():
            return int(scalar)
        else:
            return float(scalar)
    
    @staticmethod
    def __inputTransposeAxis() -> int:
        """User input the transposition axis"""
        print('1. Main diagonal')
        print('2. Side diagonal')
        print('3. Vertical line')
        print('4. Horizontal line')
        option = None
        while option not in range(5):
            option = int(input('Your choice: '))
        return option

    def __printResult(self):
        """Pretty print the resulting matrix"""
        rows = len(self.result)
        print('The result is:')
        for row in range(rows):
            print(' '.join(list(map(str, self.result[row]))))
        self.result = []
        self.uiMenu()

    def uiMenu(self):
        """Display the operations menu and wait for user selection"""
        menuOptions = {
            1: 'Add matrices',
            2: 'Multiply matrix by a constant',
            3: 'Multiply matrices',
            4: 'Transpose matrix',
            0: 'Exit'
        }
        option = None

        for number, name in menuOptions.items():
            print(f'{number}. {name}')

        while option not in range(len(menuOptions)):
            option = int(input('Your choice: '))

        if option == 0:
            sys.exit()
        elif option == 1:
            self.addMatrices()
        elif option == 2:
            self.multiplyScalar()
        elif option == 3:
            self.multiplyMatrices()
        elif option == 4:
            self.transposeMatrix()
        else:
            self.uiMenu()

    def addMatrices(self):
        """Matrix addition function"""
        try:
            self.matrixA = self.__inputMatrix('first')
            self.matrixB = self.__inputMatrix('second')

            if len(self.matrixA) != len(self.matrixB) or len(self.matrixA[0]) != len(self.matrixB[0]):
                raise ArithmeticError('The operation cannot be performed.')
        except ArithmeticError as ae:
            print(ae)
            self.uiMenu()
        else:
            rows = len(self.matrixA)
            cols = len(self.matrixA[0])

            for r in range(rows):
                row = []
                for c in range(cols):
                    row.append(self.matrixA[r][c] + self.matrixB[r][c])
                self.result.append(row)
            self.__printResult()

    def multiplyScalar(self):
        """Multiply a matrix by a number"""
        try:
            self.matrixA = self.__inputMatrix('the')
            scalar = self.__inputNumber()
        except ValueError as ve:
            print(ve)
            self.uiMenu()
        else:
            rows = len(self.matrixA)
            cols = len(self.matrixA[0])

            for r in range(rows):
                row = []
                for c in range(cols):
                    row.append(scalar * self.matrixA[r][c])
                self.result.append(row)
            self.__printResult()

    def multiplyMatrices(self):
        """Multiply two matrices"""
        try:
            self.matrixA = self.__inputMatrix('first')
            self.matrixB = self.__inputMatrix('second')

            if len(self.matrixA[0]) != len(self.matrixB):
                raise ArithmeticError('The operation cannot be performed.')
        except ArithmeticError as ae:
            print(ae)
        else:
            self.result = [[0 for col in range(len(self.matrixB[0]))] for row in range(len(self.matrixA))]

            for i in range(len(self.matrixA)):
                for j in range(len(self.matrixB[0])):
                    for k in range(len(self.matrixB)):
                        self.result[i][j] += self.matrixA[i][k] * self.matrixB[k][j]
                        self.result[i][j] = round(self.result[i][j], 3)
            self.__printResult()

    def transposeMatrix(self):
        """Matrix transposition along 4 possible axes"""
        try:
            axis = self.__inputTransposeAxis()
            self.matrixA = self.__inputMatrix('')
            self.result = \
                [[0 for col in range(len(self.matrixA[0]))] for row in range(len(self.matrixA))]
        except ValueError as ve:
            print(ve)
            self.uiMenu()
        else:
            if axis == 1:
                for i in range(len(self.matrixA)):
                    for j in range(len(self.matrixA[0])):
                        self.result[j][i] = self.matrixA[i][j]
                self.__printResult()
            elif axis == 2:
                for i in range(len(self.matrixA)):
                    for j in range(len(self.matrixA[0])):
                        self.result[i][j] = \
                            self.matrixA[len(self.matrixA[0]) - 1 - j][len(self.matrixA) - 1 - i]
                self.__printResult()
            elif axis == 3:
                for i in range(len(self.matrixA)):
                    for j in range(len(self.matrixA[0])):
                        self.result[i][j] = self.matrixA[i][len(self.matrixA[0]) - 1 - j]
                self.__printResult()
            elif axis == 4:
                for i in range(len(self.matrixA)):
                    for j in range(len(self.matrixA[0])):
                        self.result[i][j] = self.matrixA[len(self.matrixA) - 1 - i][j]
                self.__printResult()


if __name__ == '__main__':
    m = Matrices()
